package ru.t1.bugakov.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataYamlSaveFasterXmlRequest extends AbstractUserRequest {

    public DataYamlSaveFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
