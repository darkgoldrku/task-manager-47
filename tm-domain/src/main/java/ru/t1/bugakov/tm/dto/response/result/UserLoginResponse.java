package ru.t1.bugakov.tm.dto.response.result;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    @Nullable
    private String token;

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@Nullable String token) {
        this.token = token;
    }

}
