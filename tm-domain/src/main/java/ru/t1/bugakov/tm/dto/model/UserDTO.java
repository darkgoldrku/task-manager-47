package ru.t1.bugakov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.constacts.DBConstants;
import ru.t1.bugakov.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConstants.TABLE_USER)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "login")
    private String login;

    @NotNull
    @Column(nullable = false, name = "password_hash")
    private String passwordHash = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "role")
    private Role role = Role.USUAL;

    @Nullable
    @Column(name = "email")
    private String email = "";

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName = "";

    @Nullable
    @Column(name = "middle_name")
    private String middleName = "";

    @Column(name = "locked")
    private boolean locked = false;

    public UserDTO(@NotNull String login, @NotNull String passwordHash, @Nullable String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }
}