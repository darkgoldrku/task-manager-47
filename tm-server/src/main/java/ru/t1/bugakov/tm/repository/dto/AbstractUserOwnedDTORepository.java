package ru.t1.bugakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.dto.IAbstractUserOwnedDTORepository;
import ru.t1.bugakov.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedDTORepository<E extends AbstractUserOwnedModelDTO> extends AbstractDTORepository<E> implements IAbstractUserOwnedDTORepository<E> {

    public AbstractUserOwnedDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final E entity) throws Exception {
        entity.setUserId(userId);
        entityManager.persist(entity);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final E entity) {
        entityManager.merge(entity);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        entityManager.remove(entity);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull String id) {
        @Nullable final E entity = findById(userId, id);
        if (entity == null) return;
        remove(entity);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

}


