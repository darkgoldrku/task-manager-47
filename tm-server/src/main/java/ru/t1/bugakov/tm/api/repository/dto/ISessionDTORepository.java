package ru.t1.bugakov.tm.api.repository.dto;

import ru.t1.bugakov.tm.dto.model.SessionDTO;

public interface ISessionDTORepository extends IAbstractUserOwnedDTORepository<SessionDTO> {

}
