package ru.t1.bugakov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.UserDTO;

public interface IUserDTORepository extends IAbstractDTORepository<UserDTO> {

    @Nullable UserDTO findByLogin(@NotNull String login);

    @Nullable UserDTO findByEmail(@NotNull String email);

    boolean isLoginExists(@NotNull String login);

    boolean isEmailExists(@NotNull String email);

}
