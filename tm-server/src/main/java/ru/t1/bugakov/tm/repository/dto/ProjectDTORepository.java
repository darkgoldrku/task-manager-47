package ru.t1.bugakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.bugakov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();

    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@NotNull String userId) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> findAllWithSort(@NotNull String userId, @Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<ProjectDTO> criteriaQuery = criteriaBuilder.createQuery(ProjectDTO.class);
        @NotNull final Root<ProjectDTO> root = criteriaQuery.from(ProjectDTO.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("userId"), userId));
        criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public ProjectDTO findById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);

    }

    @Override
    public int getSize(@NotNull String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();

    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public @Nullable List<ProjectDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m";
        return entityManager.createQuery(jpql, ProjectDTO.class).getResultList();

    }

    @Override
    public @Nullable List<ProjectDTO> findAllWithSort(@Nullable String sortField) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<ProjectDTO> criteriaQuery = criteriaBuilder.createQuery(ProjectDTO.class);
        @NotNull final Root<ProjectDTO> from = criteriaQuery.from(ProjectDTO.class);
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(sortField)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public @Nullable ProjectDTO findById(@NotNull String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM ProjectDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();

    }

}

