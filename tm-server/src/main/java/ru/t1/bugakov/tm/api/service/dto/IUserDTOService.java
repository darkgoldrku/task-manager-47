package ru.t1.bugakov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.bugakov.tm.dto.model.UserDTO;
import ru.t1.bugakov.tm.enumerated.Role;

import java.sql.SQLException;

public interface IUserDTOService extends IAbstractDTOService<UserDTO> {

    void create(@Nullable final String login, @Nullable final String password) throws SQLException;

    void create(@Nullable final String login, @Nullable final String password, @Nullable final String email) throws SQLException;

    void create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) throws SQLException;

    @Nullable
    UserDTO findByLogin(@Nullable final String login) throws SQLException;

    @Nullable
    UserDTO findByEmail(@Nullable final String email) throws SQLException;

    void removeByLogin(@Nullable final String login) throws SQLException;

    void removeByEmail(@Nullable final String email) throws SQLException;

    void setPassword(@Nullable final String id, @Nullable String password) throws SQLException;

    void updateUser(@Nullable final String id,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName) throws SQLException;

    boolean isLoginExist(@Nullable final String login) throws SQLException;

    boolean isEmailExist(@Nullable final String email) throws SQLException;

    void lockUserByLogin(@Nullable final String login) throws SQLException;

    void unlockUserByLogin(@Nullable final String login) throws SQLException;
}
