package ru.t1.bugakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1.bugakov.tm.util.TerminalUtil;

import java.sql.SQLException;

public final class TaskUnbindToProjectCommand extends AbstractTaskCommand {

    @Override
    public void execute() throws SQLException {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getTaskEndpoint().unbindTaskToProject(new TaskUnbindFromProjectRequest(getToken(), projectId, taskId));
    }

    @NotNull
    @Override
    public String getName() {
        return "task-unbind-to-project";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Unbind task to project.";
    }

}
