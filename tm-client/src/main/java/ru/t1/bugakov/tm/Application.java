package ru.t1.bugakov.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
