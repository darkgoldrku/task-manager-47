package ru.t1.bugakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.bugakov.tm.dto.request.data.DataJsonSaveFasterXmlRequest;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        getDomainEndpoint().JsonSaveFasterXml(new DataJsonSaveFasterXmlRequest(getToken()));
    }

    @Override
    public @NotNull
    String getName() {
        return "data-save-json-fasterxml";
    }

    @Override
    public @NotNull
    String getDescription() {
        return "Save data to json file with fasterxml";
    }

}
